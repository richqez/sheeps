<html ng-app="sheepshop">
<head>
	<title>Free Ecomm Template Website Template | Home :: w3layouts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


	<link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
	<link href="public/front_assets/css/style.css" rel="stylesheet" type="text/css" media="all"/>
	<link rel="stylesheet" href="public/front_assets/css/etalage.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">



	<script type="text/javascript" src="public/front_assets/js/jquery-1.9.0.min.js"></script> 
	<script src="public/front_assets/js/jquery.openCarousel.js" type="text/javascript"></script>
	<script type="text/javascript" src="public/front_assets/js/easing.js"></script>
	<script type="text/javascript" src="public/front_assets/js/move-top.js"></script>


	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="public/css/materialize.min.css">
	
	<style>

		*, *:before, *:after {
			box-sizing: initial; 
		}
		

		.pagination{
			float: right;
		}

		.etalage_small_thumbs{
			height: 100px;
		}

	</style>


	<!-- Compiled and minified JavaScript -->
	<script src="public/js/materialize.min.js"></script>




	<script src="public/js/angular.min.js"></script>
	<script src="public/js/angular-animate.min.js"></script>
	<script src="public/js/angular-ui-router.min.js"></script>
	<script src="public/js/ngStorage.min.js"></script>
	<script src="public/js/dirPagination.js"></script>
	<script src="public/js/loading-bar.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-filter/0.5.8/angular-filter.js"></script>
	<script src="public/js/spin.min.js"></script>
	<script src="public/js/angular-spinner.js"></script>



	<script src="public/front_assets/app/app.js"></script>
	<script src="public/front_assets/app/config.js"></script>
	<script src="public/front_assets/app/constant.js"></script>

	<script src="public/front_assets/app/directive/a.js"></script>

	<script src="public/front_assets/app/service/product.js"></script>
	<script src="public/front_assets/app/service/producttype.js"></script>
	<script src="public/front_assets/app/service/selectType.js"></script>
	<script src="public/front_assets/app/service/cart.js"></script>
	<script src="public/front_assets/app/service/order.js"></script>
	<script src="public/front_assets/app/service/member.js"></script>
	
	<script src="public/front_assets/app/controller/showproduct.js"></script>
	<script src="public/front_assets/app/controller/menu.js"></script>
	<script src="public/front_assets/app/controller/showdetail.js"></script>
	<script src="public/front_assets/app/controller/cart.js"></script>
	<script src="public/front_assets/app/controller/checkout.js"></script>
	<script src="public/front_assets/app/controller/member.js"></script>
	<script src="public/front_assets/app/controller/register.js"></script>

</head>
<body>
	<div id ="center" style="position:fixed;top:50%;left:50% ; z-index:3">
		<span us-spinner="{radius:30, width:8, length: 16,scale:3,line:7}" spinner-key="spinner-1" style="z-index:10"></span>
	</div>
	<div class="header">
		<div class="wrap">
			<div class="header_top">
				<div class="logo">
					<a href="index.html"><img src="public/front_assets/images/logo.png" alt="" /></a>
				</div>

				<div class="clear"></div>
			</div> 

			<div class="navigation">
				<a class="toggleMenu" href="#">Menu</a>
				<ul class="nav">
					<li>
						<a href="#/index">Home</a>
					</li>
					<li>
						<a href="#/register">Register</a>
					</li>
					<li>
						<a href="#/checkout">CheckOut</a>
					</li>
					<li ng-include="'public/front_assets/app/template/cart.html'" ng-controller="CartController as vm" style="float:right">
						
					</li>
				</ul>
				<span class="left-ribbon"> </span> 
				<span class="right-ribbon"> </span>    
			</div>


			<!-- <div class="header_bottom">
				<div class="slider-text">
					<h2>Lorem Ipsum Placerat <br/>Elementum Quistue Tunulla Maris</h2>
					<p>Vivamus vitae augue at quam frigilla tristique sit amet<br/> acin ante sikumpre tisdin.</p>
				</div>
				<div class="slider-img">
					<img src="public/front_assets/images/slider-img.png" alt="" />
				</div>
				<div class="clear"></div>
			</div> -->
		</div>
	</div>
	<!------------End Header ------------>

	<div class="main" ui-view></div>

	<div class="footer">
		<div class="wrap">	
			<div class="copy_right">
				<p>Copy rights (c). All rights Reseverd | Template by  <a href="http://w3layouts.com" target="_blank">W3Layouts</a> </p>
			</div>	
			<div class="footer-nav">
				<ul>
					<li><a href="#">Terms of Use</a> : </li>
					<li><a href="#">Privacy Policy</a> : </li>
					<li><a href="contact.html">Contact Us</a> : </li>
					<li><a href="#">Sitemap</a></li>
				</ul>
			</div>		
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<a href="#" id="toTop"> </a>
	<script type="text/javascript" src="public/front_assets/js/navigation.js"></script>
</body>
</html>


<script src="public/front_assets/js/jquery.etalage.min.js"></script>
