<!DOCTYPE html>
<html ng-app="sheepAdmin">
    <head>
        <title>Sheeps</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta http-equiv="Cache-Control" content="no-cache" />
        
        
        <style>
            .fade.ng-enter {
              transition: 0.25s;
              opacity: 0; }
            .fade.ng-enter-active {
              opacity: 1; }
            .fade.ng-leave {
              transition: 0.25s;
              opacity: 1; }
            .fade.ng-leave-active {
              opacity: 0; }
        </style>

 
        <link rel="stylesheet" href="public/css/loading-bar.css">
        <link rel="stylesheet" href="public/css/uikit.almost-flat.min.css">
        <link rel="stylesheet" href="public/css/font-awesome.min.css">
        <link rel="stylesheet" href="public/admin_assets/style.css">
        <link rel="stylesheet" href="public/css/components/progress.almost-flat.min.css">



        <script src="public/js/jquery-2.1.4.min.js"></script>
        <script src="public/js/uikit.min.js"></script>

        <script src="public/js/angular.min.js"></script>
        <script src="public/js/angular-animate.min.js"></script>
        <script src="public/js/angular-ui-router.min.js"></script>
        <script src="public/js/ngStorage.min.js"></script>
        <script src="public/js/dirPagination.js"></script>
        <script src="public/js/loading-bar.js"></script>
        <script src="public/js/ng-file-upload-all.min.js"></script>
    

        <script src="public/admin_assets/app.js"></script>

        <script src="public/admin_assets/directive/openDialog.js"></script>
        <script src="public/admin_assets/directive/openConfrim.js"></script>

        <script src="public/admin_assets/service/auth.js"></script>
        <script src="public/admin_assets/service/product.js"></script>
        <script src="public/admin_assets/service/producttype.js"></script>
        <script src="public/admin_assets/service/user.js"></script>
        <script src="public/admin_assets/service/imges.js"></script>
        
        <script src="public/admin_assets/controller/navbar.js"></script>
        <script src="public/admin_assets/controller/producttype.js"></script>
        <script src="public/admin_assets/controller/product.js"></script>
        <script src="public/admin_assets/controller/signin.js"></script>
        <script src="public/admin_assets/controller/producttype-modal.js"></script>
       
       
        

    </head>
    
    <body>
        
        <div ui-view></div>

    </body>
</html>
