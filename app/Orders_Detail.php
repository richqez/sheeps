<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders_Detail extends Model
{
    protected $table = 'orders_detail';
    protected $fillable = ['ref_order','product_id','qty','total'];
    public $timestamps = false;
}
