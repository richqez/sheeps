<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['code', 'name','detail','price','product_type'];

    public function images(){
    	return $this->hasMany('App\Product_Images');
    }
}
