<?php 

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as IlluminateResponse;

/**
 * summary
 */
class UserController extends Controller
{
   
		public function find($id){
			return response()->json(User::find($id));
		}
		
		public function find_all(){	
			return response()->json(User::all());
		}
		
		public function create(Request $resquest){
			$user = User::create($resquest->all());
			return response()->json($user);
		}

		public function update(Request $resquest){

		}
		
		public function delete($id){
			$user = User::delete($id);
			return response()->json($user);
		}

}


 ?>