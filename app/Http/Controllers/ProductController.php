<?php 

namespace App\Http\Controllers;
use DB;
use App\Product;
use App\Product_Thumnail;
use App\Product_Images;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as IlluminateResponse;

/**
 * summary
 */
class ProductController extends Controller
{
    public function find($id){
        $product = Product::find($id);
        $product["images"] =Product_Images::where('ref_product_id',$id)->get();
    	return response()->json($product);
    }

    public function find_all(){
    	    $products = DB::select('SELECT products.id , products.code , products.name , products.detail, products.product_type , products.price, product_thumnail.path 
            FROM products 
            INNER JOIN product_thumnail 
            ON product_thumnail.ref_product_id = products.id');
            return response()->json($products);
    }
    public function create(Request $request){
    	//dd($request->all());
        $data = $request->all() ;
        $newProduct = $data['product'];
        $newImages = $data['images'];
        $newThumnail = $data['thumnail'];


        $product = Product::create($newProduct);

        $produdctId = $product->id ;    

        for($i=0 ; $i<sizeof($newImages);$i++){
            Product_Images::create(["path" => $newImages[$i] , 
                "ref_product_id" => $produdctId]);
        }

        Product_Thumnail::create(["path" => $newThumnail , 
            "ref_product_id" => $produdctId]);


        return response()->json(["message"=>"insert product success"]);


    }

    public function findByType($id){

         $products = DB::select("SELECT products.id , products.code , products.name , products.detail, products.product_type , products.price, product_thumnail.path 
            FROM products 
            INNER JOIN product_thumnail 
            ON product_thumnail.ref_product_id = products.id
            WHERE products.product_type = '$id' ");
        //$products = Product::where('product_type','=',$id)->get();
        return response()->json($products);
    }

    public function update(Request $resquest){

    }

    public function delete($id)
    {
    	$product = Product::delete($id);
    	return response()->json($product);
    }
    
}



 ?>