<?php 

namespace App\Http\Controllers;

use App\Product_Type;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as IlluminateResponse;

/**
 * summary
 */
class ProductTypeController extends Controller
{
    public function find($id){
    	return response()->json(Product_Type::find($id));
    }

    public function find_all(){
    	return response()->json(Product_Type::all());
    }

    public function create(Request $resquest){
    	$product_type = Product_Type::create($resquest->all());
    	return response()->json($product_type);
    }

    public function update(Request $resquest){
        $producttype = Product_Type::find($resquest->input('id'));
        $producttype->name = $resquest->input('name');
        $producttype->description = $resquest->input('description');
        $producttype->save();
        return response()->json($producttype);
    }

    public function delete($id)
    {
    	$product_type = Product_Type::find($id);
        $product_type->delete();
    	return response()->json($product_type);
    }
}



 ?>