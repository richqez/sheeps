<?php 

namespace App\Http\Controllers;
use DB;
use App\Orders;
use App\Orders_Detail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as IlluminateResponse;
use PHPMailer;


/**
 * summary
 */
class OrderController extends Controller
{
    public function find($id){
        $order = Orders::find($id);
        return response()->json($order);
    }

    public function find_all(){
    	    $products = DB::select('SELECT products.id , products.code , products.name , products.detail, products.product_type , products.price, product_thumnail.path 
            FROM products 
            INNER JOIN product_thumnail 
            ON product_thumnail.ref_product_id = products.id');
            return response()->json($products);
    }

    public function create(Request $request){
        
        $rawData = $request->all();
        //var_dump($rawData);
        //
        $mailhtml = '';
        $orderTotal = $rawData["total"];
        $products = $rawData["products"];
        $paymentType = $rawData["paymentype"];
        $shippingType = $rawData["shippingtype"];
        $userid = $rawData["userid"];
        $address = $rawData["address"];
        $email = $rawData["email"];
        $unSaveOrder = [
            "userid" => $userid,
            "total" => $orderTotal,
            "payment_type" => $paymentType,
            "shiping_type" => $shippingType,
            "address" => $address
        ];

  
        $mailhtml ='<table border="1">
                        <tr>
                            <th>#</th>
                            <th>ProductName</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Total</th>
                        </tr>';


        $savedOrder = Orders::create($unSaveOrder);


              $mailhead =  '<table>
                                 <tr><td>ProductID</td><td>'.'PO0000'.$savedOrder["id"].'</td></tr>
                                 <tr><td>GrandTotal</td><td>'.$rawData["total"].'</td></tr>
                            </table>' .
                            '<h3>Address : </h3>' . $address . '</br>';


        $i = 0 ;

        foreach ($products as $key => $value) {
            $unSaveOrderDetail = [
                "ref_order" => $savedOrder["id"],
                "product_id" => $value["id"],
                "qty" => $value["qty"],
                "total" => $value["qty"] * $value["price"]
            ];

            $savedOrderDetail = Orders_Detail::create($unSaveOrderDetail);

            $i++;


            $mailhtml .=   '<tr>
                            <th>'.$i.'</th>
                            <th>'. $value["name"]. '</th>
                            <th>'.$value["price"].'</th>
                            <th>'.$value["qty"].'</th>
                            <th>'.$value["qty"] * $value["price"].'</th>
                        </tr>';


        }

                       

        $mailhtml = $mailhtml . '</table>';  
                    



        $mail = new PHPMailer;
        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 2;
        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        $mail->Host = 'smtp.gmail.com';
        // use
        // $mail->Host = gethostbyname('smtp.gmail.com');
        // if your network does not support SMTP over IPv6
        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587;
        //Set the encryption system to use - ssl (deprecated) or tls
        $mail->SMTPSecure = 'tls';
        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;
        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = "dodhonatty@gmail.com";
        //Password to use for SMTP authentication
        $mail->Password = "natty55555";
        //Set who the message is to be sent from
        $mail->setFrom('Sheep@shopp.com', 'Admin SheepShop');
        //Set an alternative reply-to address
        $mail->addReplyTo('replyto@example.com', 'First Last');
        //Set who the message is to be sent to
        $mail->addAddress($email, 'Customer');
        //Set the subject line
        $mail->Subject = 'SheepShop PurchaseOrder';
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->msgHTML($mailhead . $mailhtml);




        //Replace the plain text body with one created manually
        $mail->AltBody = 'This is a plain-text message body';
        //Attach an image file
        $mail->addAttachment('images/phpmailer_mini.png');
        //send the message, check for errors
        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }

        




        return response()->json($savedOrder);
    }
    
    
}



 ?>