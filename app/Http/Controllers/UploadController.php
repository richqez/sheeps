<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response as IlluminateResponse;
use Input;
use DateTime;
/**
 * summary
 */
class UploadController extends Controller
{
   		

		public function uploadImage(Request $request){
			$image = $request->file('file');

			$extension = $image->getClientOriginalExtension();
			$dt =  new DateTime();
			$fileName = $dt->format('YmdHis').'-r'.rand(1111111, 9999999) . '.' . $extension;


			$destinationPath = 'public/images' ;
			if (!$image->move($destinationPath,$fileName)) {
				return response()->json(['message'=>"Error saving the file"],400);
			}
			return response()->json(['image_name'=> $fileName],200);
		}

		public function removeFile($filename){

			unlink('public/images/'.$filename);
			return response()->json(['message'=>'remove file ' .$filename]);
		}

		public function uploadThumnail(Request $request){

			$image = $request->file('file');

			$extension = $image->getClientOriginalExtension();
			$dt =  new DateTime();
			$fileName = $dt->format('YmdHis').'-r'.rand(1111111, 9999999) . '.' . $extension;

			$destinationPath = 'public/thumnail' ;
			if (!$image->move($destinationPath,$fileName)) {
				return response()->json(['message'=>"Error saving the file"],400);
			}
			return response()->json(['image_name'=> $fileName],200);
			
		}

		public function removeThumnail($filename){
			unlink('public/thumnail/'.$filename);
			return response()->json(['message'=>'remove file ' .$filename]);
		}



}


 ?>