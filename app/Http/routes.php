<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('front');
});


Route::get('/admin',function(){
	return view('admin');
});



$router->group(['prefix'=>'api','middleware' => 'cors'], function($router) {

	$router->post('login','AuthenticateController@authenticate');

	$router->get('product/{id}','ProductController@find');
	$router->get('product','ProductController@find_all');
	$router->get('product/type/{id}','ProductController@findByType');

	

	/**
	 * User 
	 */
	$router->get('user/{id}','UserController@find');
	$router->get('user','UserController@find_all');
	$router->post('user','UserController@create');
	$router->delete('user/{id}','UserController@delete');


	/**
	 * Product_type
	 */
	$router->get('product_type/{id}','ProductTypeController@find');
	$router->get('product_type','ProductTypeController@find_all');
	$router->post('product_type','ProductTypeController@create');
	$router->put('product_type','ProductTypeController@update');
	$router->delete('product_type/{id}','ProductTypeController@delete');

	/**
	 * Prodcut
	 */
	
	$router->post('product','ProductController@create');
	$router->delete('product/{id}','ProductController@delete');
	

	/**
	 * Upload
	 */
	$router->post('upload/image','UploadController@uploadImage');
	$router->delete('upload/image/{filename}','UploadController@removeFile');

	$router->post('upload/thumnail','UploadController@uploadThumnail');
	$router->delete('upload/thumnail/{filename}','UploadController@removeThumnail');
	

	/**
	 *  Order
	 */	

	$router->post('order','OrderController@create');


});