<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{


	protected $table = 'orders';

	protected $fillable = ['userid','total', 'payment_type', 'order_state','shiping_type','address'];

    public function Details(){
    	return $this->hasMany('APP\Orders_Detail');
    }


}
