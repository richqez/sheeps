<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Thumnail extends Model
{
    protected $table = 'product_thumnail';

    protected $fillable = ['path', 'ref_product_id'];

}
