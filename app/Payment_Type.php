<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_Type extends Model
{
    protected $table = 'payment_type';
}
