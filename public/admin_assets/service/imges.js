angular
	.module('sheepAdmin')
	.factory('ImagesService',ImagesService);

	function ImagesService($http,urls){

		var service = {
			remove        : remove,
			removeThumnail:removeThumnail
		}

		return service ;

		function remove(filename){
			return $http.delete(urls.BASE_API+'/upload/image/'+filename)
				.then(getDataComplate,getDataFaild);
		}


		function removeThumnail(filename){
			return $http.delete(urls.BASE_API+'/upload/thumnail/'+filename)
				.then(getDataComplate,getDataFaild);
		}


		function getDataComplate(response){
			return response.data ;
		}

		function getDataFaild(error){
			console.log("XHR FAILD productService");
		}
	}