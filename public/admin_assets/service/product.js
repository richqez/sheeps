angular
	.module('sheepAdmin')
	.factory('ProductService',ProductService);

	function ProductService($http,urls){
		
		var service = {
			create  : create,
			update  : update,
			remove  : remove,
			find_all: find_all,
			find    : find
		}

		return service ;

		function create(product){
			return $http.post(urls.BASE_API+'/product',product)
				 .then(getDataComplate,getDataFaild);
		}

		function update(data){
			return $http.put(urls.BASE_API+'/product/'+data.id,data)
						.then(getDataComplate,getDataFaild);
		}

		function remove(id){
			return $http.delete(urls.BASE_API+'/product/'+id)
				 		.then(getDataComplate,getDataFaild);
		}

		function find_all(){
			return $http.get(urls.BASE_API+'/product')
						.then(getDataComplate,getDataFaild);
		}

		function find(id){
			return $http.get(urls.BASE_API+'/product/'+id)
				 		.then(getDataComplate,getDataFaild);
		}

		function getDataComplate(response){
			return response.data ;
		}

		function getDataFaild(error){
			console.log("XHR FAILD productService");
		}


	}