angular
	.module('sheepAdmin')
	.factory('UserService',UserService);

	function UserService ($http,urls){
		
		var service = {
			create  : create,
			update  : update,
			remove  : remove,
			find_all: find_all,
			find    : find
		}

		return service ;

		function create(user,success,error){
			$http.post(urls.BASE_API+'/user',user)
				.success(success)
				.error(error);
		}

		function update(user,success,error){
			$http.put(urls.BASE_API+'/user/'+user.id,user)
				.success(success)
				.error(error);
		}

		function remove(id,success,error){
			$http.delete(urls.BASE_API+'/user/'+id)
				.success(success)
				.error(error);
		}

		function find_all(success,error){
			$http.get(urls.BASE_API+'/user')
				.success(success)
				.error(error);
		}

		function find(id,success,error){
			$http.get(urls.BASE_API+'/user/'+id)
				.success(success)
				.error(error);
		}


	}