
angular
	.module('sheepAdmin')
	.factory('AuthService',AuthService);

	function AuthService($http,$localStorage,urls){
    
		var service = {
			signin : signing,
			signout : signout,
			getTokenClaims :getClaimsFromToken

		}

		return service;

		function signing(user,success,error){
			$http.post(urls.BASE_API+'/login',user)
				.success(success)
				.error(error);
		}

		function signout(success)
		{
			delete $localStorage.token;
			success();
		}

		function urlBase64Decode(str) {
           var output = str.replace('-', '+').replace('_', '/');
           switch (output.length % 4) {
               case 0:
                   break;
               case 2:
                   output += '==';
                   break;
               case 3:
                   output += '=';
                   break;
               default:
                   throw 'Illegal base64url string!';
           }
           return window.atob(output);
       }

       function getClaimsFromToken() {
           var token = $localStorage.token;
           var user = {};
           if (typeof token !== 'undefined') {
               var encoded = token.split('.')[1];
               user = JSON.parse(urlBase64Decode(encoded));
           }
           return user;
       }


	}
		

