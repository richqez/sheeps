angular
	.module('sheepAdmin')
	.factory('ProductTypeService',ProductTypeService);

	function ProductTypeService ($http,urls){
		
		var service = {
			create  : create,
			update  : update,
			remove  : remove,
			find_all: find_all,
			find    : find
		}

		return service ;

		function create(product_type){
			return $http.post(urls.BASE_API+'/product_type',product_type)
						.then(getDataComplate,getDataFaild);
		}

		function update(product_type){
			return $http.put(urls.BASE_API+'/product_type/',product_type)
						.then(getDataComplate,getDataFaild);
		}

		function remove(id){
			return $http.delete(urls.BASE_API+'/product_type/'+id)
						.then(getDataComplate,getDataFaild);
		}

		function find_all(){
			return $http.get(urls.BASE_API+'/product_type')
						.then(getDataComplate,getDataFaild);

		}

		function find(id){
			return $http.get(urls.BASE_API+'/product_type/'+id)
						.then(getDataComplate,getDataFaild);
		
		}

		function getDataComplate(response){
			return response.data ;
		}

		function getDataFaild(error){
			console.log("XHR FAILD productService");
		}


	}