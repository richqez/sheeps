
angular.module('sheepAdmin')
		.controller('SigninController',SigninController);

function SigninController(AuthService,$localStorage,$location,$state){

	var vm = this ;
	vm.user = {};

	vm.signin = function(){
		AuthService.signin(vm.user,function(response){
			$localStorage.token = response.token;
			$state.go('mange.dash');
		},function(response){
			alert("Login Error");
		});
	}



}

