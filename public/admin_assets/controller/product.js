
angular.module('sheepAdmin')
		.controller('ProductController',ProductController);

function ProductController(ProductService,ProductTypeService,$localStorage,Upload,$timeout,urls,ImagesService,$state){

	var vm            = this ;
	vm.producttypes   = [] ;
	vm.product        = {} ;
	vm.products       = [] ;
	vm.dtPageSize     = 10 ;
	vm.dtCurrentPage  = 1 ;
	vm.save           = save ;
	vm.images         = [];
	vm.uploadFiles    = uploadFiles ;
	vm.removeImg      = removeImg;
	vm.uploadThumnail = uploadThumnail;
	vm.thumnail       = false;
	vm.removeThumnail = removeThumnail ;

	getProducts();
	getProductTypes();


	function uploadThumnail(f,errfile){
		vm.thumnail = f ;
		if (f) {
			f.Upload = Upload.upload({
				url:urls.BASE_API+"/upload/thumnail",
				data:{file:f}
			});

			f.Upload.then(function(response){
				$timeout(function(){
					f.result = response.data;
					console.log(response.data);
				});

			},function(response){
				
			},function(evt){
				f.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			})

			};

	}

	function removeThumnail(f){
		ImagesService.removeThumnail(f.result.image_name)
			.then(function(){
				vm.thumnail = false ;
			})
	}

	function uploadFiles(files,errFiles){
		angular.forEach(files,function(f){
			vm.images = files ;
			f.Upload = Upload.upload({
				url:urls.BASE_API+"/upload/image",
				data:{file:f}
			});

			f.Upload.then(function(response){
				$timeout(function(){
					f.result =response.data;
					console.log(response.data);
				});
			},function(response){
				if (response.status > 0) {
					console.log(response.status + ":"  + response.data)
				};
			},function(evt){
				f.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			})
		});
	}

	function removeImg(f){
		var index = vm.images.indexOf(f);
		ImagesService.remove(f.result.image_name)
			.then(function(){
				vm.images.splice(index,1);
			});
	}


	function getProductTypes(){
		ProductTypeService.find_all()
			.then(function(producttypes){
				vm.producttypes =  producttypes ;
			})
	}

	function getProducts(){
		ProductService.find_all()
			.then(function(products){
				vm.products = products;
			}) ;
	}
	function save(){

		var images = [];

		angular.forEach(vm.images,function(i){
			images.push(i.result.image_name);
		})

		var data ={
			product : vm.product,
			images : images ,
			thumnail : vm.thumnail.result.image_name
		};

		console.log(data);
		ProductService.create(data)
			.then(function(response){
				console.log(response);

				vm.product  = {} ;
				vm.thumnail = false;
				vm.images   = [];

				$state.go('mange.product');

			})

	}

	function clear(){
		vm.product = {} ;
	}




}

