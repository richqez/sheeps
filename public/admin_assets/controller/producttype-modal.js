angular.module('sheepAdmin')
		.controller('ProductTypeModalController',ProductTypeModalController);

		function ProductTypeModalController(ProductTypeService,$scope){
			
			var vm         = this ;
			vm.save        = save;
			vm.close       = close ;
			vm.producttype = {};


			function save(){
				ProductTypeService.create(vm.producttype)
					.then(function(){
						$scope.producttypeVm.getProducttypes();
						vm.producttype ={};
					});
			}

			function close(){
				vm.producttype = {};
			}

		}