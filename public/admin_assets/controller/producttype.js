
angular.module('sheepAdmin')
		.controller('ProductTypeController',ProductTypeController);

		function ProductTypeController(ProductTypeService,$scope){
			
			var vm = this ;

			vm.producttype     = {};
			vm.producttypes    = [] ;
			vm.dtPageSize      = 10 ;
			vm.dtCurrentPage   = 1 ;
			vm.editMode        = false ;	
			vm.producttypeTemp = {};
			vm.edit            = edit ;
			vm.isEditRow       = isEditRow ;
			vm.cancel          = cancel ;
			vm.updateRow       = updateRow ;
			vm.getProducttypes = getProducttypes;
			vm.onConfrim       = onConfrim ;
			$scope.onConfrim   = onConfrim;

			getProducttypes();

			function onConfrim(key){
				ProductTypeService.remove(key)
					.then(function(){
						getProducttypes();
					});
			}

			function getProducttypes(){
				ProductTypeService.find_all()
					.then(function(response){
						vm.producttypes = response ;
					})
			}

			function edit(producttype){

				vm.editMode = vm.producttypes.indexOf(producttype);
				vm.producttypeTemp = angular.copy(producttype);
			}

			function isEditRow(producttype){
				if (producttype.id == vm.producttypeTemp.id) {
					return true;
				}else{
					return false ;
				}
			}

			function cancel(){
				vm.producttypeTemp = {};
			}

			function updateRow(){
				ProductTypeService.update(vm.producttypeTemp)
					.then(function(){
						getProducttypes();
						vm.producttypeTemp = {} ;
					})
			}




		}