
angular
 .module('sheepAdmin', 
 	[
 	'ui.router',
 	'ngStorage',
 	'angularUtils.directives.dirPagination',
 	'ngAnimate',
 	'angular-loading-bar',
 	'ngFileUpload'])

 .constant('urls',{
 	BASE_API : 'http://localhost/sheeps/api'
 })
 .config(httpConfig)
 .config(stateConfig)
 .config(pagination)


function pagination(paginationTemplateProvider){
	paginationTemplateProvider.setPath('public/admin_assets/template/dirPagination.tpl.html');
}

function httpConfig($httpProvider){
 	$httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
			   return {
			       'request': function (config) {
			           config.headers = config.headers || {};
			           if ($localStorage.token) {
			               config.headers.Authorization = 'Bearer ' + $localStorage.token;
			           }
			           return config;
			       },
			       'responseError': function (response) {
			           if (response.status === 401 || response.status === 403 || response.status === 400) {
			               $location.path('/signin');
			           }
			           return $q.reject(response);
			       }
			   }
	}]);
 }

function stateConfig($stateProvider,$urlRouterProvider){
 	$stateProvider
 		.state('signin',{
 			url:'/',
 			templateUrl:'public/admin_assets/template/signin.html',
 			controller:'SigninController',
 			controllerAs:'vm'
 		})
 		.state('mange',{
 			abstract: true,
 			templateUrl: 'public/admin_assets/template/mange.html'
 		})
 		.state('mange.dash',{
 			url: '/mange/dash',
 			templateUrl: 'public/admin_assets/template/mange.dash.html'
 		})
 		.state('mange.product',{
 			url:'/mange/product',
 			templateUrl: 'public/admin_assets/template/mange.product.html',
 			controller: 'ProductController',
 			controllerAs:'vm'
 		})
 		.state('mange.product-create',{
 			urls:'/mange/product-create',
 			templateUrl:'public/admin_assets/template/mange.product.create.html',
 			controller: 'ProductController',
 			controllerAs:'vm'
 		})
 		.state('mange.producttype',{
 			url:'/mange/producttype',
 			templateUrl: 'public/admin_assets/template/mange.producttype.html',
 			controller: 'ProductTypeController',
 			controllerAs:'producttypeVm'
 		})

 	$urlRouterProvider.otherwise("/");
 }

