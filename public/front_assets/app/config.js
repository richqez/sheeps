angular.module("sheepshop")
	.config(stateConfig)
	.config(spinnerConfig);


function stateConfig($stateProvider,$urlRouterProvider){
 	$stateProvider
 		.state('home',{
 			url:'/index',
 			templateUrl:'public/front_assets/app/template/home.html',
 		})
 		.state('product',{
 			url:'/product/:productId',
 			templateUrl:'public/front_assets/app/template/preview.html',
 			controller:'ShowDetailController',
 			controllerAs:'vm'
 		})
 		.state('checkout',{
 			url:'/checkout',
 			templateUrl:'public/front_assets/app/template/checkout.html',
 			controller:'CheckOutController',
 			controllerAs:'vm'
 		}).state('login',{
 			url:'/login',
 			templateUrl:'public/front_assets/app/template/login.html',
 			controller:'MemberController',
 			controllerAs:'vm'
 		}).state('register',{
 			url:'/register',
 			controller:'RegisterController',
 			templateUrl:'public/front_assets/app/template/register.html',
 			controllerAs: 'vm'
 		})


 	$urlRouterProvider.otherwise("/index");
 }


function spinnerConfig(usSpinnerConfigProvider){
	usSpinnerConfigProvider.setDefaults({color: '#e44f2b'});
}