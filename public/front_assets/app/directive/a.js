angular.module('sheepshop')
.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if(attrs.ngClick || attrs.href === '' || attrs.href === '#'){
                elem.on('click', function(e){
                    e.preventDefault();
                });
            }
        }
   };
})
.directive('fiZoom',function($timeout,usSpinnerService){
    return function(scope,elem,attes){
        if (scope.$last) {
                usSpinnerService.spin('spinner-1');
                $timeout(function(){
                    $('#etalage').etalage({
                        thumb_image_width: 300,
                        thumb_image_height: 400,
                        source_image_width: 900,
                        source_image_height: 1200,
                        show_hint: false,
                        click_callback: function(image_anchor, instance_id){
                            alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
                        }
                    });

                    usSpinnerService.stop('spinner-1');
                },10)

        };
    }
})
