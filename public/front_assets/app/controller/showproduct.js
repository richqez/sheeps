angular.module('sheepshop')
	.controller('ShowProductController',ShowProductController);

	function ShowProductController(ProductService,SelectTypeService,$scope,usSpinnerService,$timeout){
		var vm = this ;

		vm.products = [];
		vm.currentCat = 'All Categories';
		vm.sizeOfProducts = 0 ;
	

		getProducts();

		function getProducts(){
			showSpin();
			ProductService.find_all()
				.then(function(response){
					vm.sizeOfProducts =response.length;
					vm.products = splitProduct(response,4);
					hideSpin();
				});
		}


		$scope.$on('selectType',function(){
			showSpin();
				ProductService.findByType(SelectTypeService.getCurrent())
				.then(function(response){

					$timeout(function(){
						vm.sizeOfProducts =response.length;
						vm.products = splitProduct(response,4);
						vm.currentCat = SelectTypeService.getCurrentName();
						hideSpin();
					},100)
					
				})

			
		})


		function splitProduct(data,size){
			var newArr = [];
		      for (var i=0; i<data.length; i+=size) {
		          newArr.push(data.slice(i, i+size));
		      }
		   return newArr;
		}


		function showSpin(){
			usSpinnerService.spin('spinner-1');
		}

		function hideSpin(){
			usSpinnerService.stop('spinner-1');
		}

	}