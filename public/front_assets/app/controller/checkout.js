angular.module('sheepshop')
	.controller('CheckOutController',CheckOutController);

	function CheckOutController($scope,CartService,OrderService,MemberService,$state,$localStorage,usSpinnerService){
		var vm = this;
		vm.minusqty = minusqty;
		vm.plusqty = plusqty;
		vm.cart = {};
		vm.vat = 0 ;
		vm.cart.total = 0 ;
		vm.grandtotal = 0 ;
		vm.sendOrder = sendOrder;
		init();

		$scope.$on('cartUpdate',function(){
			vm.cart = CartService.getCart();
			calVat();
		})

		function minusqty(product){
			CartService.minusqty(product);
		}

		function plusqty(product){
			CartService.plusqty(product);
		}

		function init(){
			vm.cart = CartService.getCart();
			calVat();
			console.log(vm.cart);
		}

		function calVat(){

			if (typeof vm.cart !== 'undefined' ) {
				vm.vat = vm.cart.total * 7 /100 ;
				vm.grandtotal =  vm.cart.total + vm.vat ;
			}
		}

		function sendOrder(){
			if (typeof $localStorage.token !== 'undefined') {
					if (typeof vm.cart !== 'undefined' && vm.cart.products.length > 0 && vm.address !== '') {
					var tokenClaims = MemberService.getTokenClaims();
					//console.log(tokenClaims.sub);

					var order = CartService.getCart();
					
					order.address = vm.address ;
					order.paymentype = 1;
					order.shippingtype = 1;
					order.userid = tokenClaims.sub;
					order.email = vm.email ;


					usSpinnerService.spin('spinner-1');
					OrderService.create(order)
						.then(function(){
							CartService.destroy();
							vm.vat = 0 ;
							vm.grandtotal = 0 ;
							vm.address = null;
							vm.email = null;
							usSpinnerService.stop('spinner-1');
							alert("OrderComplate !!!! \n plase check email");
						},function(){
							usSpinnerService.stop('spinner-1');
							alert("error");

						})

				}else{
					alert("Can't ORDER !!!!!!");
				}
			}else{
				alert("go to login");
				$state.go('login');
			}

		}

	}
