angular.module('sheepshop')
	.controller('MenuController',MenuController);

	function MenuController(ProductTypeService,SelectTypeService){
		var vm = this ;
		vm.menus = [];
		vm.selectMenu = selectMenu;
		getMenus();

		function selectMenu(type){
			SelectTypeService.setCurrent(type.id);
			SelectTypeService.setCurrentName(type.name)
		}

		function getMenus(){
			ProductTypeService.find_all()
				.then(function(response){
					vm.menus = response ;
				})
		}
	}