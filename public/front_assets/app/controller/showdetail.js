angular.module('sheepshop')
	.controller('ShowDetailController',ShowDetailController);

	function ShowDetailController ($stateParams,ProductService,CartService){


		var vm = this ;
		vm.addToCart = addToCart ;
		vm.product = {};


		getProduct();

		function getProduct(){
			ProductService.find($stateParams.productId)
				.then(function(response){
					vm.product = response ;
				});
		}

		function addToCart(){

			var product = angular.copy(vm.product);
			product.qty = 1 ;
			CartService.addToCart(product);
			alert("add product to cart");
		}


		

	}