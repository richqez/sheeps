angular.module('sheepshop')
	.controller('CartController',CartController);

	function CartController($scope,CartService){
		var vm = this ;

		vm.cart = {};

		vm.productsize = 0 ;
		

		update();

		$scope.$on('cartUpdate',function(){
			
			update();

		});

		function update()
		{
			vm.cart = CartService.getCart();
			if (typeof vm.cart !== 'undefined' ) {
				vm.productsize = vm.cart.products.length;
			}else{
				vm.productsize =  0;
			}
			
		}

	}