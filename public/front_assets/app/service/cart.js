angular
	.module('sheepshop')
	.factory('CartService',CartService);

	function CartService($localStorage,$rootScope){

		var service = {
			addToCart : addToCart ,
			removeFromCart : removeFromCart,
			getCart:getCart,
			plusqty:plusqty,
			destroy:destroy,
			minusqty:minusqty

		}


		return service ;


		function addToCart(product){
			
			var inCart = false ;

			if (typeof $localStorage.cart == 'undefined') {

				var cart = {
					total : 0,
					products :[]
				}

				$localStorage.cart = cart;
			};

			var products = $localStorage.cart.products;

			for (var i = 0; i < products.length; i++) {
				if (products[i].id == product.id) {
					products[i].qty +=1;
					inCart = true;
					break;
				};
			};

			if (!inCart) {
				products.push(product);
			};


			cal();
		}

		function cal(){
			var products = $localStorage.cart.products;
			var cart = $localStorage.cart;
			var total = 0 ;
			for (var i = 0; i < products.length; i++) {
				total+= products[i].price * products[i].qty;
			};
			cart.total =total;
			screenUpdate();
		}

		function removeFromCart(product){
			var products = $localStorage.cart.products;
			var index = products.indexOf(product);
			screenUpdate();
		}

		function minusqty(product){
			var products = $localStorage.cart.products;
			var index = products.indexOf(product);
			
			if (products[index].qty <=1) {
				products.splice(index,1);
			}else{
				products[index].qty -=1;
			}

			cal();
		}

		function plusqty(product){
			var products = $localStorage.cart.products;
			var index = products.indexOf(product);
			products[index].qty +=1;
			cal();
		}

		function getCart(){
			return $localStorage.cart;
		}

		function destroy(){
			delete $localStorage.cart;
			screenUpdate();
		}

		function screenUpdate(){
			$rootScope.$broadcast('cartUpdate');
		}


	}