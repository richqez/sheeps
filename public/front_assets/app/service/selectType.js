angular
	.module('sheepshop')
	.factory('SelectTypeService',SelectTypeService);

	function SelectTypeService($rootScope){
		var service = {
			getCurrent : getCurrent,
			getCurrentName:getCurrentName,
			setCurrent : setCurrent,
			setCurrentName:setCurrentName,
		}
		return service ;

		var currentTypeSelect = "";
		var currentTypeSelectName = "";


		function getCurrent(){
			return this.currentTypeSelect;
		}

		function getCurrentName(){
			return this.currentTypeSelectName;
		}

		function setCurrent(typeid){
			this.currentTypeSelect = typeid;
			$rootScope.$broadcast('selectType');
		}

		function setCurrentName(name){
			this.currentTypeSelectName = name ;
		}


	}