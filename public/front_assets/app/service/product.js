angular
	.module('sheepshop')
	.factory('ProductService',ProductService);

	function ProductService($http,urls,SelectTypeService){
		
		var service = {
			find_all: find_all,
			find    : find,
			findByType:findByType
		}

		return service ;

		function find_all(){
			return $http.get(urls.BASE_API+'/product')
						.then(getDataComplate,getDataFaild);
		}

		function find(id){
			return $http.get(urls.BASE_API+'/product/'+id)
				 		.then(getDataComplate,getDataFaild);
		}

		function findByType(typeid){
			return $http.get(urls.BASE_API+'/product/type/'+typeid)
						.then(getDataComplate,getDataFaild);
		}


		function getDataComplate(response){
			return response.data ;
		}

		function getDataFaild(error){
			console.log("XHR FAILD productService");
		}




	}