angular
	.module('sheepshop')
	.factory('ProductTypeService',ProductTypeService);

	function ProductTypeService ($http,urls){
		
		var service = {
			find_all: find_all
		}

		return service ;


		function find_all(){
			return $http.get(urls.BASE_API+'/product_type')
						.then(getDataComplate,getDataFaild);

		}


		function getDataComplate(response){
			return response.data ;
		}

		function getDataFaild(error){
			console.log("XHR FAILD productService");
		}


	}