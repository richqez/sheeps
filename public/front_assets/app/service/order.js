angular
	.module('sheepshop')
	.factory('OrderService',OrderService);

	function OrderService($http,urls,CartService){
		var service = {
			create : create
		};
		return service ;

		function create(order){
			return $http.post(urls.BASE_API+'/order',order)
						.then(getDataComplate,getDataFaild);
		}

		function getDataComplate(response){
			return response.data ;
		}

		function getDataFaild(error){
			console.log("XHR FAILD productService");
		}

	}