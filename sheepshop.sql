-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 06, 2016 at 06:48 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sheep_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `total` float(11,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `payment_type` int(11) NOT NULL,
  `order_state` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'wait',
  `shiping_type` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `userid`, `total`, `created_at`, `payment_type`, `order_state`, `shiping_type`, `updated_at`, `address`) VALUES
(22, 4, 287500.00, '2016-01-06 05:09:40', 1, 'wait', 1, '2016-01-06 05:09:40', 'sadsadddddddddddddddddddd'),
(23, 4, 287500.00, '2016-01-06 05:10:22', 1, 'wait', 1, '2016-01-06 05:10:22', 'xzcxzcxzcxzxcxz'),
(24, 4, 25000.00, '2016-01-06 05:12:53', 1, 'wait', 1, '2016-01-06 05:12:53', 'dsdsdsds'),
(25, 4, 25000.00, '2016-01-06 05:27:19', 1, 'wait', 1, '2016-01-06 05:27:19', 'fdsfdsfdsfds'),
(26, 4, 300000.00, '2016-01-06 05:53:08', 1, 'wait', 1, '2016-01-06 05:53:08', 'fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff'),
(27, 4, 25000.00, '2016-01-06 06:20:56', 1, 'wait', 1, '2016-01-06 06:20:56', 'dasdsadsaaaaaaaaaaaaaaaaaaaaaaa'),
(28, 4, 2125060.00, '2016-01-06 06:32:09', 1, 'wait', 1, '2016-01-06 06:32:09', 'hghg'),
(29, 4, 25000.00, '2016-01-06 09:12:50', 1, 'wait', 1, '2016-01-06 09:12:50', 'sadsadsad'),
(30, 4, 25000.00, '2016-01-06 09:14:51', 1, 'wait', 1, '2016-01-06 09:14:51', 'sadsa'),
(31, 4, 25000.00, '2016-01-06 09:44:49', 1, 'wait', 1, '2016-01-06 09:44:49', 'dsadsa'),
(32, 4, 37500.00, '2016-01-06 09:46:48', 1, 'wait', 1, '2016-01-06 09:46:48', 'dasdsa'),
(33, 4, 37500.00, '2016-01-06 09:48:47', 1, 'wait', 1, '2016-01-06 09:48:47', 'sadsadsa'),
(34, 4, 12.00, '2016-01-06 10:13:59', 1, 'wait', 1, '2016-01-06 10:13:59', 'dsadsadsadsadsad'),
(35, 4, 25000.00, '2016-01-06 10:17:36', 1, 'wait', 1, '2016-01-06 10:17:36', 'dsadasdsadsad'),
(36, 4, 25000.00, '2016-01-06 10:17:36', 1, 'wait', 1, '2016-01-06 10:17:36', 'dsadasdsadsad');

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail`
--

CREATE TABLE `orders_detail` (
  `id` int(11) NOT NULL,
  `ref_order` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` float(7,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders_detail`
--

INSERT INTO `orders_detail` (`id`, `ref_order`, `product_id`, `qty`, `total`) VALUES
(32, 22, 45, 7, 87500.00),
(33, 22, 44, 8, 99999.99),
(34, 23, 45, 7, 87500.00),
(35, 23, 44, 8, 99999.99),
(36, 24, 44, 1, 25000.00),
(37, 25, 44, 1, 25000.00),
(38, 26, 44, 12, 99999.99),
(39, 27, 44, 1, 25000.00),
(40, 28, 44, 82, 99999.99),
(41, 28, 46, 5, 60.00),
(42, 28, 45, 6, 75000.00),
(43, 29, 44, 1, 25000.00),
(44, 30, 44, 1, 25000.00),
(45, 31, 44, 1, 25000.00),
(46, 32, 44, 1, 25000.00),
(47, 32, 45, 1, 12500.00),
(48, 33, 44, 1, 25000.00),
(49, 33, 45, 1, 12500.00),
(50, 34, 46, 1, 12.00),
(51, 35, 44, 1, 25000.00),
(52, 36, 44, 1, 25000.00);

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `name`, `description`, `updated_at`, `created_at`) VALUES
(1, 'Bank_tranfer', 'x', '2016-01-06 06:23:12', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `detail` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `price` float(7,2) NOT NULL,
  `product_type` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `code`, `name`, `detail`, `price`, `product_type`, `created_at`, `updated_at`) VALUES
(44, 'xzczxc', 'xc', 'xzcxzcxzcx', 25000.00, 1, '2016-01-02 03:08:11', '2016-01-02 03:08:11'),
(45, 'sasad', 'sdsa', 'sadsadsa', 12500.00, 1, '2016-01-03 11:51:52', '2016-01-03 11:51:52'),
(46, 'dsdsd', 'sdss', 'd', 12.00, 5, '2016-01-06 05:55:41', '2016-01-06 05:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `path` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ref_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `path`, `created_at`, `updated_at`, `ref_product_id`) VALUES
(14, '20160102100807-r3954785.png', '2016-01-02 03:08:11', '2016-01-02 03:08:11', 44),
(15, '20160102100808-r7213107.png', '2016-01-02 03:08:11', '2016-01-02 03:08:11', 44),
(16, '20160102100808-r8876701.png', '2016-01-02 03:08:11', '2016-01-02 03:08:11', 44),
(17, '20160103185144-r6592800.jpg', '2016-01-03 11:51:52', '2016-01-03 11:51:52', 45),
(18, '20160103185144-r7423391.jpg', '2016-01-03 11:51:52', '2016-01-03 11:51:52', 45),
(19, '20160106125535-r7226718.jpg', '2016-01-06 05:55:41', '2016-01-06 05:55:41', 46);

-- --------------------------------------------------------

--
-- Table structure for table `product_thumnail`
--

CREATE TABLE `product_thumnail` (
  `id` int(11) NOT NULL,
  `path` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ref_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_thumnail`
--

INSERT INTO `product_thumnail` (`id`, `path`, `created_at`, `updated_at`, `ref_product_id`) VALUES
(5, '20160102100802-r9838485.png', '2016-01-02 03:08:11', '2016-01-02 03:08:11', 44),
(6, '20160103185138-r9889294.jpg', '2016-01-03 11:51:52', '2016-01-03 11:51:52', 45),
(7, '20160106125526-r7676291.jpg', '2016-01-06 05:55:41', '2016-01-06 05:55:41', 46);

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'กระเป๋า', 'กระเป๋าทุกประเภท', '2015-12-16 07:41:16', '2015-12-16 00:41:16'),
(5, 'เครื่องดื่มชูกำลัง', 'm150,Spark', '2015-12-17 17:38:38', '2015-12-17 10:38:38'),
(10, 'อาหารสัตว์', 'อาหารสัตว์นำเข้า', '2015-12-18 20:26:19', '2015-12-18 13:26:19'),
(11, 'ของเล่นเด็ก', 'ของเล่นเด็กนำเข้าจากประเทศจีน', '2015-12-18 13:26:40', '2015-12-18 13:26:40'),
(12, 'เครื่องแต่งกาย หญิง', 'ไม่ทราบ', '2015-12-22 11:03:43', '2015-12-22 04:03:43'),
(13, 'เครื่องแต่งกาย ชาย', 'producttype', '2015-12-22 11:03:53', '2015-12-22 04:03:53'),
(14, 'ของฝาก', 'producttype', '2015-12-22 11:04:01', '2015-12-22 04:04:01'),
(15, 'producttype', 'producttype', '2015-12-18 13:27:09', '2015-12-18 13:27:09'),
(16, 'producttype', 'producttype', '2015-12-18 13:27:12', '2015-12-18 13:27:12'),
(17, 'producttype', 'producttype', '2015-12-18 13:27:16', '2015-12-18 13:27:16'),
(18, 'producttype', 'producttype', '2015-12-18 13:27:20', '2015-12-18 13:27:20');

-- --------------------------------------------------------

--
-- Table structure for table `shiping_type`
--

CREATE TABLE `shiping_type` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` float(7,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shiping_type`
--

INSERT INTO `shiping_type` (`id`, `name`, `price`, `created_at`, `updated_at`) VALUES
(1, 'ems', 100.00, '2016-01-06 06:23:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` binary(60) NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `firstname`, `lastname`, `address`, `role`, `created_at`, `updated_at`) VALUES
(4, 'a@a.com', 0x243279243130244d75626476717a584f465343787a72596d6a7370564f446450526875575470416e6b734d5776552e2e4f6630724a4678504f515432, 'xx', 'xx', '123/5', '', '2015-12-14 23:01:03', '2015-12-14 23:01:03'),
(5, 'a@a.com', 0x2432792431302430695a4249737447655034566270697175385162314f5937586a446d5275364c724d6b336b3567334f45416f56784e4750796a6569, '213', '231', '231213', '', '2016-01-06 08:27:39', '2016-01-06 08:27:39'),
(6, 'a', 0x2432792431302474316e49573155366865786f796a4f424d39354730657236566a31625354764f6c57676b305567347737376b55502f77676c633853, 'a', 'a', 'a', '', '2016-01-06 08:29:57', '2016-01-06 08:29:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shiping_type` (`shiping_type`),
  ADD KEY `payment_type` (`payment_type`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `orders_detail`
--
ALTER TABLE `orders_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref_order` (`ref_order`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_type` (`product_type`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref_product_id` (`ref_product_id`);

--
-- Indexes for table `product_thumnail`
--
ALTER TABLE `product_thumnail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ref_product_id` (`ref_product_id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shiping_type`
--
ALTER TABLE `shiping_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `orders_detail`
--
ALTER TABLE `orders_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `product_thumnail`
--
ALTER TABLE `product_thumnail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `shiping_type`
--
ALTER TABLE `shiping_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`shiping_type`) REFERENCES `shiping_type` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`payment_type`) REFERENCES `payment_type` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`userid`) REFERENCES `user` (`id`);

--
-- Constraints for table `orders_detail`
--
ALTER TABLE `orders_detail`
  ADD CONSTRAINT `orders_detail_ibfk_1` FOREIGN KEY (`ref_order`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_detail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`product_type`) REFERENCES `product_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`ref_product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `product_thumnail`
--
ALTER TABLE `product_thumnail`
  ADD CONSTRAINT `product_thumnail_ibfk_1` FOREIGN KEY (`ref_product_id`) REFERENCES `products` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;